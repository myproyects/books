package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Book;
import com.example.demo.repository.BookReporitory;

@RestController
@RequestMapping("/api/book")
public class BookController {

    @Autowired
    private BookReporitory bookReporitory;

    @GetMapping("/getAllItems")
    public ResponseEntity<List<Book>> getAllItems(@RequestParam(required = false) String author) {
        List<Book> books = new ArrayList<Book>();
        try {
            if (author == null || author.isEmpty()) {
                books = (List<Book>) bookReporitory.findAll();
            } else {
                books = bookReporitory.findByAuthor(author);
            }
            if (books.isEmpty()) {
                return new ResponseEntity<List<Book>>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<List<Book>>(books, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<List<Book>>(books, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping("/getItemByHigherPrice/{price}")
    public ResponseEntity<List<Book>> getItemByHigherPrice(@RequestParam(value = "price", required = true) String price) {
        List<Book> books = bookReporitory.findItemByHigherPrice(price);
        if (books.isEmpty()) {
            return new ResponseEntity<List<Book>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<Book>>(books, HttpStatus.OK);
    }

    @RequestMapping("/getItemByHigherPrice/{price}")
    public ResponseEntity<List<Book>> getItemByLowerPrice(@RequestParam(value = "price", required = true) String price) {
        List<Book> books = bookReporitory.findItemByLowerPrice(price);
        if (books.isEmpty()) {
            return new ResponseEntity<List<Book>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<Book>>(books, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Book> getBook(@PathVariable("id") long id) {
        Optional<Book> book = bookReporitory.findById(id);
        if (book.isPresent()) {
            return new ResponseEntity<Book>(book.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<Book>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/save")
    public ResponseEntity<Book> saveBook(@RequestBody Book book) {
        try {
            Book bookP = bookReporitory.save(new Book(book.getName(), book.getAuthor(), book.getPrice()));
            return new ResponseEntity<Book>(bookP, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<Book>(book, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/update/{id}")
    public ResponseEntity<Book> updateBook(@PathVariable("id") long id, @RequestBody Book book) {
        try {
            Optional<Book> bookP = bookReporitory.findById(id);
            if (bookP.isPresent()) {
                Book bookUpd = bookP.get();
                bookUpd.setName(book.getName());
                bookUpd.setAuthor(book.getAuthor());
                bookUpd.setPrice(book.getPrice());
                return new ResponseEntity<Book>(bookReporitory.save(bookUpd), HttpStatus.OK);
            } else {
                return new ResponseEntity<Book>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<Book>(book, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<HttpStatus> deleteBook(@PathVariable("id") long id) {
        try {
            bookReporitory.deleteById(id);
            return new ResponseEntity<HttpStatus>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<HttpStatus>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
