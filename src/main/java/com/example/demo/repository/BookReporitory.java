package com.example.demo.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.example.demo.model.Book;

public interface BookReporitory extends CrudRepository<Book, Long> {

    List<Book> findItemByHigherPrice(String price);

    List<Book> findItemByLowerPrice(String price);

    List<Book> findByAuthor(String author);

}
