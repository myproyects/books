package com.example.demo.model;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

import lombok.AllArgsConstructor;
import lombok.Data;

import org.springframework.data.annotation.Id;

@Data
@AllArgsConstructor
public class Book {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String name;
    private String author;
    private String price;
    
    public Book(String name2, String author2, String price2) {
        this.name = name2;
        this.author = author2;
        this.price = price2;
    }

}
